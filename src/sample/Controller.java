package sample;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TextField;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Controller {
    public javafx.scene.control.Spinner<Integer> Spinner;
    @FXML private Button ButtonStopServer;
    @FXML private Tab Tab1;
    @FXML private ListView<String> ListViewClients;
    @FXML private PasswordField Password;
    @FXML private TextField Login;
    private Socket inSocket;
    private DataInputStream inStream;
    private DataOutputStream outStream;
    private final int LOGIN = 30001, LOGINAC = 30004;
    private final int LCLIENTS = 30002, LCLIENTSA = 30005;
    private final int STOP = 30003, STOPA = 30006;
    private final int PORTADMIN = 80001;
    private int transaction;
    @FXML
    private void initialize() throws IOException {
        inSocket = new Socket("127.0.0.1", PORTADMIN);
        outStream = new DataOutputStream(inSocket.getOutputStream());
        inStream=new DataInputStream(inSocket.getInputStream());
        ButtonStopServer.setDisable(true);
        Spinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 20, 5));
    }
    public void ConnectionButton(ActionEvent actionEvent) throws IOException {
        if(Login.getText().length() == 0 || Password.getText().length() == 0) AlertBox("LOGIN", "Votre mot de passe ou login est null");
        else
        {
            outStream.writeByte(LOGIN);//Nom de la transaction
            outStream.writeBytes(Login.getText());
            outStream.writeBytes(Password.getText());
            transaction = inStream.readInt();
            if(transaction == LOGINAC)
            {
                Tab1.setDisable(false);
            }
            else AlertBox("LOGIN", "Votre mot de passe ou login est incorrecte");

        }
    }

    public void ShutdownServer(ActionEvent actionEvent) throws IOException {
        outStream.writeByte(STOP);
        outStream.writeByte(Spinner.getValue());
        transaction = inStream.readInt();
        if(transaction == STOPA) AlertBox("STOP", "Le shutdown s'est parfaitement réalisé");
        else AlertBox("STOP", "Problème lors du shutdown");
    }

    public void ListeClients(ActionEvent actionEvent) throws IOException {
        ListViewClients.getItems().clear();
        outStream.writeByte(LCLIENTS);
        transaction = inStream.readInt();
        if(transaction == LCLIENTSA)
        {
            int nbClients = inStream.readInt();
            List<String> ipClients = new ArrayList<String>();
            for(int i = 0; i < nbClients; i++)
                ipClients.add(inStream.readUTF());
            ListViewClients.setItems(FXCollections.observableArrayList(ipClients));
            ButtonStopServer.setDisable(false);

        }
        else AlertBox("LCLIENTS", "Probleme lors de la reception des clients");
    }

    private void AlertBox(String title, String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }
}
